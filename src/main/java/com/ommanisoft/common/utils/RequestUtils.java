package com.ommanisoft.common.utils;

import com.ommanisoft.common.utils.values.HttpResponse;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

public class RequestUtils {
  private static RestTemplate restTemplate;
  /**
   * 5 second
   */
  private static int timeout = 5;

  private static RestTemplate getTemplate() {
    if (restTemplate != null) {
      return restTemplate;
    }
    restTemplate = new RestTemplate(getClientHttpRequestFactory(timeout));
    return restTemplate;
  }

  public static <T> HttpResponse sendRequest(
    HttpMethod method, String requestUrl, T mData, Map<String, ?> headerParam) {
    return sendRequest(method, requestUrl, mData, headerParam, new HashMap<>());
  }

  public static <T> HttpResponse sendRequest(
    HttpMethod method, String requestUrl, T mData, Map<String, ?> headerParam, Map<?, ?> uriVariables) {
    try {
      RestTemplate restTemplate = getTemplate();
      HttpHeaders headers = new HttpHeaders();

      if (headerParam != null) {
        for (Map.Entry<String, ?> entry : headerParam.entrySet()) {
          headers.add(entry.getKey(), entry.getValue().toString());
        }
      }
      if (uriVariables == null) {
        uriVariables = new HashMap<>();
      }
      if (headers.get("Content-Type") == null) {
        headers.add("Content-Type", "application/json");
      }

      HttpEntity<?> data = null;
      if (headerParam.get("Content-Type") == null) {
        headers.add("Content-Type", "application/json");
        data = new HttpEntity<T>(mData, headers);
      } else if (headerParam.get("Content-Type").equals("application/x-www-form-urlencoded")) {
        Map<String, Object> formData;
        formData = JsonParser.objectToMap(mData);
        data = new HttpEntity<>(formData, headers);
      }
      ResponseEntity<String> response =
        restTemplate.exchange(requestUrl, method, data, String.class, uriVariables);

      return new HttpResponse(response.getStatusCode(), response.getBody(), response.getHeaders());
    } catch (HttpClientErrorException hex) {
      return new HttpResponse(
        hex.getStatusCode(), hex.getResponseBodyAsString(), hex.getResponseHeaders());
    } catch (Exception e) {
      throw new RuntimeException("Restemplace err: {}", e);
    }
  }

  private static SimpleClientHttpRequestFactory getClientHttpRequestFactory(int timeOut) {
    SimpleClientHttpRequestFactory clientHttpRequestFactory = new SimpleClientHttpRequestFactory();
    // Connect timeout
    clientHttpRequestFactory.setConnectTimeout(timeOut * 1000);

    // Read timeout
    clientHttpRequestFactory.setReadTimeout(timeOut * 1000);
    return clientHttpRequestFactory;
  }
}
